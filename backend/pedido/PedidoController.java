package com.provider.it.api.pedido;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class PedidoController {

    private PedidoService service;

    @Autowired
    public PedidoController(PedidoService service) {
        this.service = service;
    }

    @GetMapping("/pedido/{id}")
    public ResponseEntity<PedidoVO> consultar(@PathVariable Long id) {
        return ResponseEntity.ok(service.consultar(id));
    }

    @GetMapping("/pedido")
    public Page<PedidoVO> listar(Pageable pageable) {
        return service.listar(pageable);
    }

    @PutMapping("/pedido")
    public ResponseEntity<PedidoVO> criar(@Valid @RequestBody PedidoRequest request) {
        return ResponseEntity.ok(service.criar(request));
    }


    @DeleteMapping("/pedido/{id}")
    public ResponseEntity<PedidoVO> alterar(@PathVariable Long id) {
        return ResponseEntity.ok(service.excluir(id));
    }

}