package com.provider.it.pedido;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class PedidoService {

    private PedidoRepository repository;

    @Autowired
    public PedidoService(PedidoRepository repository) {
        this.repository = repository;
    }

    public PedidoVO consultar(Long idPedido) {
        Pedido pedido = getAndValidateExistence(idPedido);
        return new PedidoVO.Builder(pedido.getId(), pedido.getNome(), pedido.getDescricao()).build();
    }

    public Page<PedidoVO> listar(Pageable pageable) {
        return repository.findAll(pageable)
                .map((pedido) -> new PedidoVO.Builder(pedido.getId(), pedido.getNome(), pedido.getDescricao()).build());
    }

    public PedidoVO criar(PedidoRequest request) {
		Pedido pedido = repository.save(new Pedido.Converter(request).convert());
        return new PedidoVO.Builder(pedido.getId(), pedido.getNome(), pedido.getDescricao()).build();
    }

    public PedidoVO alterar(PedidoRequest request, Long id) {
        getAndValidateExistence(id);
        Pedido pedido = repository.save(new Pedido.Converter(request).convert());
        return new PedidoVO.Builder(pedido.getId(), pedido.getNome(), pedido.getDescricao()).build();
    }

    public PedidoVO excluir(Long id) {
        Pedido pedido = getAndValidateExistence(id);
        repository.delete(id);
        return new PedidoVO.Builder(pedido.getId(), pedido.getNome(), pedido.getDescricao()).build();
    }

    private Pedido getAndValidateExistence(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Not found for [" + id + "]"));
    }

}
