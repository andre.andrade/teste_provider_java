package com.provider.it.api.pedido;

import javax.persistence.*;

@Entity
public class Pedido {

    private  Long id;
    private  String nome;
    private  String descricao;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Column(length = 65535, columnDefinition="Text")
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Pedido(PedidoRequest request){
        this.setId(request.getId());
        this.setNome(request.getNome());
        this.setDescricao(request.getDescricao());
    }

    public static class Converter {
        private PedidoRequest request;

        public Converter(PedidoRequest request) {
            this.request = request;
        }

        public Pedido convert() {

            return new Pedido(request);
        }
    }
}
