package com.provider.it.pedido;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class PedidoRequest {

    private  Long id;
    private  String nome;
    private  String descricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Valid
    @NotNull
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
