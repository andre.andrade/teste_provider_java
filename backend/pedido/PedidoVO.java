package com.provider.it.pedido;

public class PedidoVO {

    private final Long id;
    private final String nome;
    private final String descricao;

    public PedidoVO(Builder builder) {
        this.id = builder.id;
        this.nome = builder.nome;
        this.descricao = builder.descricao;
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public static class Builder {

        private final Long id;
        private final String nome;
        private final String descricao;

        public Builder(Long id, String nome, String descricao) {
            this.id = id;
            this.nome = nome;
            this.descricao = descricao;
        }

        public PedidoVO build(){
            return new PedidoVO(this);
        }
    }
}
