package com.provider.it.pedido;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PedidoRepository  extends PagingAndSortingRepository<Pedido, Long>, JpaSpecificationExecutor<Pedido> {

}
