import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';

import { Pokemon } from '../model';
import { PokemonService } from '../pokemon.service';

@Injectable({
  providedIn: 'root'
})
export class DetailResolver implements Resolve<Pokemon> {

  constructor(
    private pokemonService: PokemonService,
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Pokemon> {
    const name = route.params.name;
    if (name) {
      return this.pokemonService.get(name);
    } else {
      return of(new Pokemon());
    }
  }
}