import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Pokemon } from '../model';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  pokemon: Pokemon;

  constructor(
    private activatedRoute: ActivatedRoute,
    private serivce: PokemonService
  ) { }

  ngOnInit() {
    this.pokemon = this.activatedRoute.snapshot.data.pokemon;
  }

}
