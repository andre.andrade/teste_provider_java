export class PokemonResult {
  name: string;
  url: string;
}

export class Pokemon {
  name: string;
  abilities: any[];
  forms: any[];
  game_indices: any[];
  types: any[];
  stats: any[];
  sprites: {
    front_default: string;
    back_shiny_female: string;
    back_default: string;
  };
}

export interface Pageable<T> {
  results: Array<T>;
  count: number;
  previous: string;
  next: string;
}


