import { Component, OnInit } from '@angular/core';

import { PokemonResult } from '../model';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  pokemons: PokemonResult[];
  searchText;

  constructor(public service: PokemonService) { }

  ngOnInit() {
    this.service.list().subscribe(
      (pokemons) => this.pokemons = pokemons);
  }

}
