import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Pageable, Pokemon, PokemonResult } from './model';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  URL_BASE = 'http://pokeapi.co/api/v2';

  constructor(
    private http: HttpClient,
  ) { }

  list(): Observable<PokemonResult[]> {
    return this.http.get<Pageable<PokemonResult>>(`${this.URL_BASE}/pokemon?limit=1050`)
      .pipe(
        map((response: Pageable<PokemonResult>) => response.results)
      );
  }

  get(name: string): Observable<Pokemon> {
    return this.http.get<Pokemon>(`${this.URL_BASE}/pokemon/${name}`);
  }

}
