import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { DetailComponent } from './detail/detail.component';
import { DetailResolver } from './detail/detail.resolver';
import { ListComponent } from './list/list.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent
  },
  {
    path: ':name',
    component: DetailComponent,
    resolve: {
      pokemon: DetailResolver
    }
  }
];

@NgModule({
  declarations: [
    ListComponent,
    DetailComponent
  ],
  imports: [
    Ng2SearchPipeModule,
    CommonModule,
    BrowserModule, 
    FormsModule,
    RouterModule.forRoot(routes),
  ]
})
export class PokemonModule { }
